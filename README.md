# Südsicht HUGO STARTER KIT (https://gohugo.io)

## Getting Started

### Prerequisites

make sure hugo extended is installed

### 1. Clone Project in your local GitLab-Projects-Folder
```
git clone https://gitlab.com/suedsicht/hugo-starter.git new_project_name
```

### 2. Create new Project on GitLab.com
- Click on GitLab.com the + Icon > New project
- Enter your "new_project_name" > Create

### 3. Rename Origin
```
cd new_project_name
git remote rename origin old-origin
git remote add origin https://gitlab.com/suedsicht/new_project_name.git
git push -u origin --all
git push -u origin --tags
```

### 4. SFTP-Variables
- Change SFTP-HOST, SFTP-USERNAME, SFTP-PASSWORT on GitLab.com > Your Project > Settings > CI-CD > Variables

### 5. Icons, Logo and OpenGraph-Image
- Create a SVG-Icon (260x260px)
- Go to https://realfavicongenerator.net and upload the SVG-Icon
- Choose Path "/images/icons/favicons"
- Download the Facicon package and extract the content to "/static/images/icons/favicons"
- Replace the HTML-Tags in "/layouts/partials/favicons.html"
- Change logo.png in "/static/images/schema_opengraph/logo.png"
- Change the OpenGraph-Image in  "/static/images/schema_opengraph/opengraph.jpg"

## 6. hugo Commands
1. Start server
`hugo-server`
2. Build site
`hugo`

## 7. Deploy Website on GitLab Pages
- Visit GitLab.com
- Choose your Project
- CI / CD
- Pipelines
- Click the second hook
- Click Play-Button at deploy_pages

## 8. Deploy Website on Netlify
Tutorial will follow

## 9. Tools we use
7-1 SASS-Boilerplate <br />
https://github.com/HugoGiraudel/sass-boilerplate
<br /><br />
ASCII-Generator<br />
http://www.network-science.de/ascii (Fonts: big + small)
<br /><br />
SASS Mixins<br />
http://zerosixthree.se/8-sass-mixins-you-must-have-in-your-toolbox <br />
https://engageinteractive.co.uk/blog/top-10-scss-mixins
<br /><br />
RealFaviconGenerator<br />
https://realfavicongenerator.net