---
title: "{{ replace .Name "-" " " | title }}"
slug: "{{ replace .Name "-" " " | title }}"
description: ""
draft: true #or false to render
tags: ["tag1","tag2","tag3"]
categories: ["cat1"]
keywords: 
author: ""
pagetopic: "page"
image: "opengraph.jpg"
date: "{{ .Date }}"
lastmod: "{{ .Date }}"
schematype: "WebPage" #WebPage, Article, BlogPost
type: "page"
layout: "single"
robots: "noindex, nofollow"
---