---
title: "{{ replace .Name "-" " " | title }}"
slug: "{{ replace .Name "-" " " | title }}"
description: ""
draft: true #or false to render
tags: ["tag1","tag2","tag3"]
categories: ["cat1"]
keywords: 
author: ""
pagetopic: "blog"
image: "opengraph.jpg"
publishdate: "{{ .Date }}"
#expiryDate: "{{ .Date }}"
lastmod: "{{ .Date }}"
schematype: "BlogPost" #WebPage, Article, BlogPost
type: "blog"
layout: "single"
robots: "noindex, nofollow"
---