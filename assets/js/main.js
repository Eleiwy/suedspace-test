//       _                   _____           _       _   
//      | |                 / ____|         (_)     | |  
//      | | __ ___   ____ _| (___   ___ _ __ _ _ __ | |_ 
//  _   | |/ _` \ \ / / _` |\___ \ / __| '__| | '_ \| __|
// | |__| | (_| |\ V / (_| |____) | (__| |  | | |_) | |_ 
//  \____/ \__,_| \_/ \__,_|_____/ \___|_|  |_| .__/ \__|
//                                            | |        
//                                            |_|


/*------------------------------------*\
  NOTE: 01. Responsive Menu
        Inspired by https://github.com/bradtraversy/modern_portfolio
\*------------------------------------*/

// Select DOM Items
const menuBtn = document.querySelector('.menu_btn');
const menu = document.querySelector('.menu_container');
const menuItems = document.querySelector('.menu_items');
const menuBranding = document.querySelector('.menu_branding');
const menuItem = document.querySelectorAll('.menu_item');

// Set Initial State Of Menu
let showMenu = false;

menuBtn.addEventListener('click', toggleMenu);
menuItem.forEach(el => el.addEventListener('click', toggleMenu))

function toggleMenu() {
  if (!showMenu) {
    menuBtn.classList.add('close');
    menu.classList.add('show');
    menuItems.classList.add('show');
    menuBranding.classList.add('show');
    menuItem.forEach(item => item.classList.add('show'));

    // Set Menu State
    showMenu = true;
  } else {
    menuBtn.classList.remove('close');
    menu.classList.remove('show');
    menuItems.classList.remove('show');
    menuBranding.classList.remove('show');
    menuItem.forEach(item => item.classList.remove('show'));

    // Set Menu State
    showMenu = false;
  }
}

/*------------------------------------*\
  NOTE: 02. Tiny-Slider
        https://github.com/ganlanyuan/tiny-slider
        Active script in baseof.html
\*------------------------------------*/
/*
var slider = tns({
  container: '#slider .slides',
  autoplay: false,
  autoplayButton: false,
  autoplayButtonOutput: false,
  controlsContainer: '#slider .controls'
});

/* For Multiple Tiny-Slider */
/*
i = 0;
$('.tinyslider').each(function() {
  ++i;
  var slider = tns({
    container: '#slider'+i,
    items: 1,
    autoplay: true,
    nav: false,
    controlsContainer: '#controls'+i,
    autoplayButtonOutput: false
  });
});
*/
/*------------------------------------*\
  NOTE: 03. Change CSS on Scroll
        http://www.mattmorgante.com/technology/sticky-navigation-bar-javascript
        Active script in baseof.html
\*------------------------------------*/

const navT = document.querySelector('header');
const navB = document.querySelector('footer');
const navTop = navT.offsetTop + 80;
const navBottom = navB.offsetTop + 150;
const menuBtnLine = document.querySelectorAll('.menu_btn_line');
const mobileStickyHeader = document.querySelector('.mobile_sticky_header');
const btnTop = document.querySelector('.btnTop');
const menuBtnContainer = document.querySelector('.menu_btn_container');

function stickyNavigation() {

  if (window.scrollY >= navTop) {
    menuBtnLine.forEach(item => item.classList.add('black'));
    mobileStickyHeader.classList.add('show');
    menu.classList.add('sticky');
    btnTop.classList.add('show');
  } else {
    menuBtnLine.forEach(item => item.classList.remove('black'));
    mobileStickyHeader.classList.remove('show');
    menu.classList.remove('sticky');
    btnTop.classList.remove('show');
  }
  if ((window.innerHeight + window.scrollY + 50) >= document.body.offsetHeight) {
    btnTop.classList.add('bottom');
    menuBtnContainer.classList.add('bottom');
  } else {
    btnTop.classList.remove('bottom');
    menuBtnContainer.classList.remove('bottom');
  }
}

window.addEventListener('scroll', stickyNavigation);

/*------------------------------------*\
  NOTE: 04. Vanilla JavaScript Scroll to Anchor
        https://perishablepress.com/vanilla-javascript-scroll-anchor/
        Add class 'scroll' to HTML-Element to activate
\*------------------------------------*/

(function() {
	scrollTo();
})();

function scrollTo() {
	const links = document.querySelectorAll('.scroll');
	links.forEach(each => (each.onclick = scrollAnchors));
}

function scrollAnchors(e, respond = null) {
	const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
	e.preventDefault();
	var targetID = (respond) ? respond.getAttribute('href') : this.getAttribute('href');
	const targetAnchor = document.querySelector(targetID);
	if (!targetAnchor) return;
	const originalTop = distanceToTop(targetAnchor);
	window.scrollBy({ top: originalTop, left: 0, behavior: 'smooth' });
	const checkIfDone = setInterval(function() {
		const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
		if (distanceToTop(targetAnchor) === 0 || atBottom) {
			targetAnchor.tabIndex = '-1';
			targetAnchor.focus();
			window.history.pushState('', '', targetID);
			clearInterval(checkIfDone);
		}
	}, 100);
}