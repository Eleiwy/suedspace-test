---
title: "Artikel 1"
slug: "artikel1"
description: "Das ist ein Blogartikel"
draft: false #or true to not render
tags: ["tag1","tag2","tag3"]
categories: ["cat1"]
keywords: artikel1, deutsch, blog, neuigkeiten
author: "Daniel"
pagetopic: "blog"
image: "opengraph.jpg"
publishdate: "2018-01-18T12:50:39+01:00"
#expiryDate: "2050-09-18T12:50:39+01:00"
lastmod: "2018-01-19T15:01:05+01:00"
schematype: "BlogPost" #WebPage, Article, BlogPost
type: "blog"
layout: "single"
robots: "index, follow"
---
Hallo das ist Artikel 1 von letztem Jahr!