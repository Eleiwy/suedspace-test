---
title: "Südspace Coworking"
description: "Willkommen beim hugo-starter von Südsicht Medien GmbH"
draft: false #or true to not render
tags: 
categories: 
keywords: start, template, hugo
author: "Me"
pagetopic: "welcome"
image: "opengraph.jpg"
date: "2019-09-19T12:50:39+01:00"
lastmod: "2019-09-19T15:01:05+01:00"
schematype: "WebPage" #WebPage, Article, BlogPost

greeting: "Hallo Herr"
name: "Müller"

welcomeText: "Coworking Space
in einem Kreativen Umfeld"
teamText: "Schon <span class='yellow_text'>ab 7 EUR </span>pro Tag kannst Du dabei sein"

bekommst: "Das bekommst du von uns"
mitbringst: "Das bringst du mit"
wir:
  job1:
    name: "WLAN"
    service: "Gigabit-LAN und 5GHz WLAN im gesamten Büro für eine stabile Verbindung."
  job2:
    name: "BEEPBUUP"
    service: "Faxen und Drucken ist bei uns inklusive."
  job3:
    name: "Privatsphäre"
    service: "Eine Telefonzelle, die dich total von der Außenwelt isoloert"
du:
  job1:
    name: "Geräte"
    
  job2:
    name: "Equipment"
    
  job3:
    name: "Freude"
    
lockere_atmosphaere: "Lockere Atmosphäre"
lockere_atm_text: "in so einer lässigen Atmosphäre hast du die Möglichkeit zu chillen, wobei wir uns in einer kulturellen und akademischen Umgebung befinden.
Kulturhaus und DHBW sind bei uns leicht zu erreichen."

meetingsraum_headline: "schoener Meetingsraum"
meetingsraum_text: "in so einer lässigen Atmosphäre hast du die Möglichkeit zu chillen, wobei wir uns in einer kulturellen und akademischen Umgebung befinden.
Kulturhaus und DHBW sind bei uns leicht zu erreichen."
essen_headline: "Einkaufs- und Essenmöglilchkeiten"
essen_text: "REWE ist unser Freund  und das Restaurant Amicus ist unser Nachbar. Dir steht auch unsere Küche zur  Verfügung."

leistungenHeadline: "Unsere Leistungen im Überblick"

leistungen:
  leistung1:
    name: "SEO"
  leistung2:
    name: "Webdesign"
  leistung3:
    name: "Onlineshop"
  leistung4:
    name: "Responsive Webdesign"
  leistung5:
    name: "UX/UI"
  leistung6:
    name: "Google Analytics"
  leistung7:
    name: "Social Media"
  leistung8:
    name: "Apps"
  leistung9:
    name: "3D-Animation"
  leistung10:
    name: "Animatics"
  leistung11:
    name: "2D-Animation"
  leistung12:
    name: "Image-Film"
  leistung13:
    name: "Erklärfilm"
  leistung14:
    name: "Konzeption"
  
belegungsplan: "Belegungsplan"
erreichbarkeit: "So sind wir zu erreichen"
kontaktdaten: "mail@suedsicht.de<br>+49 7541 288 99 55 0<br>Fallenbrunen 17, D 880045 Friedrichshafen"
---
