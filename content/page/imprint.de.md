---
title: "Impressum"
slug: "impressum"
description: "Unser Impressum"
draft: false #or true to not render
tags: 
categories: 
keywords: impressum, kontakt, adresse
author: "Ramin+Daniel"
pagetopic: "imprint"
image: "opengraph.jpg"
date: "2019-09-19T12:50:39+01:00"
lastmod: "2019-09-19T15:01:05+01:00"
schematype: "WebPage" #WebPage, Article, BlogPost
type: "page"
layout: "single"
robots: "noindex, nofollow"
---
<h2>Angaben gemäß § 5 TMG:</h2>
<p>Südsicht Medien GmbH<br />
Fallenbrunnen 17<br />
88045  Friedrichshafen</p>

<h2>Vertreten durch:</h2>
<p>Herr Benjamin Presser, Geschäftsführer<br />
Herr Benjamin Sambolec, Geschäftsführer</p>

<h2>Kontakt:</h2>
<p>Telefon: +49 7541 2899554<br />
Telefax: +49 7541 2899559<br />
E-Mail: kontakt@suedsicht.de</p>

<h2>Registereintrag:</h2>
<p>Eintragung im Handelsregister. <br />
Registergericht:Amtsgericht Ulm <br />
Registernummer: HRB 732448</p>

<h2>Umsatzsteuer:</h2>
<p>Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:<br />
DE 302012218</p>

<h2>Streitschlichtung</h2>
<p>Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit: <a href="https://ec.europa.eu/consumers/odr" target="_blank">https://ec.europa.eu/consumers/odr</a>.<br /> Unsere E-Mail-Adresse finden Sie oben im Impressum.</p>

<p>Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</p>

<h3>Haftung für Inhalte</h3> <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p> <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p> <h3>Haftung für Links</h3> <p>Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p> <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p> <h3>Urheberrecht</h3> <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</p> <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>

<h2>Genutzte Fotos von</h2>
<ul>
  <li><a href="https://unsplash.com/photos/08bOYnH_r_E" title="‘Tis the season of rhubarb. And strawberry. And blood orange. Praise be. Amen." alt="Link zu Brooke Lark's picture" target="_blank">Brooke Lark</a> bei <a href="https://www.unsplash.com/" title="Unsplash.com" alt="Link zu Unsplash.com" target="_blank">Unsplash</a></li>
</ul>